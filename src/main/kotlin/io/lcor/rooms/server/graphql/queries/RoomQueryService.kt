package io.lcor.rooms.server.graphql.queries

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.expediagroup.graphql.server.operations.Query
import io.lcor.rooms.server.models.Room
import io.lcor.rooms.server.models.RoomParameters
import io.lcor.rooms.server.repositories.RoomRepository
import io.lcor.rooms.server.utils.Log

class RoomQueryService : Query {

    @GraphQLDescription("Return a specific Room by it's ID")
    fun room(id: String) = RoomRepository.getRoomById(id)

    @GraphQLDescription("Return all public Rooms")
    fun rooms(): List<Room> {
        val rooms = RoomRepository.getAll()
        return rooms
    }

}