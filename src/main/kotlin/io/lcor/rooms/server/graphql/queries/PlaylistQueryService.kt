package io.lcor.rooms.server.graphql.queries

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.expediagroup.graphql.server.operations.Query
import io.lcor.rooms.server.repositories.PlaylistRepository

class PlaylistQueryService : Query {

    @GraphQLDescription("Return a specific Playlist by it's ID")
    fun playlist(id: String) = PlaylistRepository.getPlaylistById(id)

    @GraphQLDescription("Return all playlists")
    fun playlists() = PlaylistRepository.getAll()

}