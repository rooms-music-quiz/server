package io.lcor.rooms.server.graphql

import com.expediagroup.graphql.generator.SchemaGeneratorConfig
import com.expediagroup.graphql.generator.TopLevelObject
import com.expediagroup.graphql.generator.toSchema
import graphql.GraphQL
import io.lcor.rooms.server.graphql.mutations.RoomMutationService
import io.lcor.rooms.server.graphql.queries.PlaylistQueryService
import io.lcor.rooms.server.graphql.queries.RoomQueryService
import io.lcor.rooms.server.graphql.queries.TrackQueryService

private val config = SchemaGeneratorConfig(supportedPackages = listOf("io.lcor.rooms.server"))
private val queries = listOf(
    TopLevelObject(PlaylistQueryService()),
    TopLevelObject(RoomQueryService()),
    TopLevelObject(TrackQueryService()),
)
private val mutations = listOf(
    TopLevelObject(RoomMutationService())
)
private val graphQLSchema = toSchema(config = config, queries = queries, mutations = mutations)

fun getGraphQLObject(): GraphQL = GraphQL.newGraphQL(graphQLSchema).build()