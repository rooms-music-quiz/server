package io.lcor.rooms.server.graphql

import com.expediagroup.graphql.server.execution.GraphQLRequestHandler
import com.expediagroup.graphql.server.execution.GraphQLServer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.request.*

class KtorGraphQLServer(
    requestParser: KtorGraphQLRequestParser,
    contextFactory: KtorGraphQLContextFactory,
    requestHandler: GraphQLRequestHandler
) : GraphQLServer<ApplicationRequest>(requestParser, contextFactory, requestHandler) {


    companion object {

        fun getGraphQLServer(mapper: ObjectMapper): KtorGraphQLServer {
            val requestParser = KtorGraphQLRequestParser(mapper)
            val contextFactory = KtorGraphQLContextFactory()
            val graphQL = getGraphQLObject()
            val requestHandler = GraphQLRequestHandler(graphQL)

            return KtorGraphQLServer(requestParser, contextFactory, requestHandler)
        }
    }

}