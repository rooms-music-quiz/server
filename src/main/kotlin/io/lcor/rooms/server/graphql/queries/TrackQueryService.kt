package io.lcor.rooms.server.graphql.queries

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.expediagroup.graphql.server.operations.Query
import io.lcor.rooms.server.repositories.TrackRepository

class TrackQueryService : Query {

    @GraphQLDescription("Return a specific Track by it's ID")
    suspend fun track(id: String) = TrackRepository.getTrackById(id)

    @GraphQLDescription("Return all tracks")
    fun tracks() = TrackRepository.getAll()

}