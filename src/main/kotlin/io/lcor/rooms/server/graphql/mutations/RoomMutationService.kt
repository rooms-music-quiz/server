package io.lcor.rooms.server.graphql.mutations

import com.expediagroup.graphql.generator.annotations.GraphQLDescription
import com.expediagroup.graphql.server.operations.Mutation
import graphql.GraphQLException
import io.lcor.rooms.server.models.PlayerResults
import io.lcor.rooms.server.models.Room
import io.lcor.rooms.server.models.RoomParameters
import io.lcor.rooms.server.repositories.PlaylistRepository
import io.lcor.rooms.server.repositories.RoomRepository
import kotlinx.coroutines.delay

class RoomMutationService : Mutation {

    @GraphQLDescription("Create a Room with the specified parameters")
    suspend fun createRoom(params: RoomParameters): Room? {

        if (!params.isValid()) throw GraphQLException("Invalid parameters")

        return if (params.playlistId != null) {
            RoomRepository.createRoom(params.playlistId, params.isPublic!!, params.isRandom!!, params.isImmutable!!)
        } else {
            val playlist = PlaylistRepository.createPlaylist(
                params.name!!,
                params.genres?.split(",")!!,
                params.difficulty!!,
                params.description!!,
                params.numbers!!,
                params.locale,
            )
            delay(1000L)
            RoomRepository.createRoom(playlist, params.isPublic!!, params.isRandom!!, params.isImmutable!!)
        }
    }

    @GraphQLDescription("Add a player to a specific Room")
    fun addPlayerToRoom(playerId: String, roomId: String): String? = RoomRepository.addPlayerToRoom(roomId, playerId)

    @GraphQLDescription("Remove a player from a Room")
    fun removePlayerFromRoom(roomId: String, playerId: String): Boolean =
        RoomRepository.removePlayerFromRoom(roomId, playerId)

    @GraphQLDescription("Start a Room by it's ID")
    fun startRoom(roomId: String): Boolean = RoomRepository.startRoom(roomId)

    @GraphQLDescription("Update a player score in the last round of a room")
    fun updatePlayerScore(roomId: String, score: PlayerResults): Boolean =
        RoomRepository.updatePlayerScore(roomId, score)

    @GraphQLDescription("Return a score based on a player guess for a song")
    suspend fun guessTrack(
        roomId: String,
        playerId: String,
        title: String,
        artists: List<String>,
        input: String
    ): PlayerResults =
        RoomRepository.guessTrack(roomId, playerId, title, artists, input)

}