package io.lcor.rooms.server.plugins


import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import io.ktor.application.*

fun Application.configureFirebase() {

    // Auth parameters used to write securely in the db
    val auth = hashMapOf<String, Any>( "uid" to "rooms-service-worker" )

    // Configuration options
    val options = FirebaseOptions.builder()
        .setCredentials(GoogleCredentials.getApplicationDefault())
        .setDatabaseUrl("https://music-quiz-d2dad-default-rtdb.europe-west1.firebasedatabase.app")
        .setDatabaseAuthVariableOverride(auth)
        .build()

    FirebaseApp.initializeApp(options)
}