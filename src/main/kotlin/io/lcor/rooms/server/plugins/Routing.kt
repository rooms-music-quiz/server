package io.lcor.rooms.server.plugins


import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.lcor.rooms.server.graphql.KtorGraphQLServer

fun Application.configureRouting() {

    val mapper = jacksonObjectMapper()

    install(Routing) {
        routing {

            if (environment.developmentMode) {
                graphQlRoute(mapper)
                get("playground") {
                    this.call.respondText(buildPlaygroundHtml("graphql", "subscriptions"), ContentType.Text.Html)
                }
            } else {
                authenticate("auth-jwt") {
                    graphQlRoute(mapper)
                }
            }
        }
    }
}

private fun Route.graphQlRoute(mapper: ObjectMapper) {
    post("graphql") {

        val result = KtorGraphQLServer.getGraphQLServer(mapper).execute(call.request)

        if (result != null) {
            val json = mapper.writeValueAsString(result)
            call.respond(json)
        } else {
            call.respond(HttpStatusCode.BadRequest, "Invalid request")
        }

    }
}

private fun buildPlaygroundHtml(graphQLEndpoint: String, subscriptionsEndpoint: String) =
    Application::class.java.classLoader.getResource("graphql-playground.html")?.readText()
        ?.replace("\${graphQLEndpoint}", graphQLEndpoint)
        ?.replace("\${subscriptionsEndpoint}", subscriptionsEndpoint)
        ?: throw IllegalStateException("graphql-playground.html cannot be found in the classpath")