package io.lcor.rooms.server.plugins

import io.ktor.application.*
import io.lcor.rooms.server.utils.Log

fun Application.configureLogging() {
    Log.logger = log
}