package io.lcor.rooms.server.plugins


import com.auth0.jwk.JwkProviderBuilder
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import java.net.URL

fun Application.configureAuthentication() {

    install(Authentication) {

        val provider = "https://www.googleapis.com/service_accounts/v1/jwk/securetoken@system.gserviceaccount.com"
        val issuer = "https://securetoken.google.com/music-quiz-d2dad"
        val jwkProvider = JwkProviderBuilder(URL(provider))
            .build()

        jwt("auth-jwt") {
            realm = "rooms-server"
            verifier(jwkProvider, issuer)
            validate { credential ->
                val token = credential.payload.getClaim("user_id").asString()
                if (token == credential.subject) JWTPrincipal(credential.payload)
                else null
            }
        }
    }
}