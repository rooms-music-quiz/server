package io.lcor.rooms.server.models

import com.expediagroup.graphql.generator.annotations.GraphQLIgnore
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.Exclude
import com.google.firebase.database.FirebaseDatabase
import io.lcor.rooms.server.repositories.PlaylistRepository
import io.lcor.rooms.server.repositories.TrackRepository
import io.lcor.rooms.server.utils.Constants
import kotlinx.coroutines.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.util.*

@Serializable
data class Room(
    var id: String = UUID.randomUUID().toString(),
    var playlist: String? = null,
    var players: List<String> = listOf(),
    var isRandom: Boolean = false,
    var isPublic: Boolean = true,
    var isImmutable: Boolean = false,
    var state: RoomState = RoomState.LOBBY,
    var currentlyPlaying: String? = null,
    var rounds: MutableList<Round> = mutableListOf()
) {

    @Transient
    private var scope: CoroutineScope? = CoroutineScope(Dispatchers.Default)

    @Transient
    private val playedTracks = mutableListOf<String>()

    @Transient
    private lateinit var ref: DatabaseReference

    @GraphQLIgnore
    fun addPlayer(playerId: String): Boolean {
        players = players.plus(playerId)
        return true
    }

    @GraphQLIgnore
    fun removePlayer(playerId: String): Boolean {
        players = players.minus(playerId)
        return true
    }

    @Synchronized
    @GraphQLIgnore
    fun updatePlayerScore(score: PlayerResults): Boolean {
        return rounds.last().scores.run {
            removeIf { it.playerId == score.playerId }
            add(score)
        }
    }

    @Exclude
    fun playlist(): Playlist? = playlist?.let { PlaylistRepository.getPlaylistById(it) }

    @Exclude
    suspend fun currentlyPlaying(): Track? = currentlyPlaying?.let { TrackRepository.getTrackById(it) }

    @GraphQLIgnore
    fun play() {

        if (!::ref.isInitialized) {
            ref = FirebaseDatabase.getInstance("https://rooms-music-quiz-d2dad.europe-west1.firebasedatabase.app/")
                .getReference(id)
        }

        val playlist = PlaylistRepository.getPlaylistById(playlist!!)!!

        if (scope == null) scope = CoroutineScope(Dispatchers.Default)

        scope!!.launch {

            // Buffering the first song
            state = RoomState.INITIALIZATION
            currentlyPlaying = getNextTrackFromPlaylist(playlist)
            ref.updateChildrenAsync(
                mapOf(
                    "state" to state,
                    "currentlyPlaying" to currentlyPlaying,
                )
            )
            delay(Constants.DEFAULT_INITIALIZATION_DURATION)

            while (true) {

                // Initialize scores for each player in the room
                val currentRound = Round(players, currentlyPlaying()!!.artists().size)
                rounds.add(currentRound)

                // Start playing the song
                state = RoomState.PLAYING
                ref.updateChildrenAsync(
                    mapOf(
                        "state" to state,
                        "rounds" to rounds,
                    )
                )

                // Wait for the default duration of a song
                delay(Constants.DEFAULT_ROUND_DURATION)

                // TODO : Get players answers, add points...

                // The game is finished
                if (playedTracks.size == playlist.tracks.size && !isPublic) break

                // The game is finished, but the room is public so we start again
                else if (playedTracks.size == playlist.tracks.size) {
                    playedTracks.clear()
                    rounds.clear()
                }

                // Buffering the next song
                state = RoomState.BUFFERING
                currentlyPlaying = getNextTrackFromPlaylist(playlist)
                ref.updateChildrenAsync(
                    mapOf(
                        "state" to state,
                        "currentlyPlaying" to currentlyPlaying,
                    )
                )
                delay(Constants.DEFAULT_BREAK_DURATION)

                // TODO : Print scores, display rankings, track details on the client
            }

            // TODO : Save the Room somewhere
            state = RoomState.TERMINATED
            ref.child("state").setValueAsync(state)
        }
    }

    @GraphQLIgnore
    @Exclude
    fun getNextTrackFromPlaylist(playlist: Playlist, random: Boolean = true): String {
        val nextTrack = if (random) {
            playlist.tracks.minus(playedTracks.toSet()).random()
        } else {
            playlist.tracks.minus(playedTracks.toSet()).first()
        }
        playedTracks.add(nextTrack)
        return nextTrack
    }

    @GraphQLIgnore
    fun stop() {
        // Stop the main coroutine
        if (scope != null && scope!!.isActive) {
            scope!!.cancel("Room has been manually stopped")
            scope = null
        }
    }

    @GraphQLIgnore
    fun reset() {

        if (!::ref.isInitialized) {
            ref = FirebaseDatabase.getInstance("https://rooms-music-quiz-d2dad.europe-west1.firebasedatabase.app/")
                .getReference(id)
        }

        stop()

        // Restart Room
        state = RoomState.LOBBY
        currentlyPlaying = null
        rounds.clear()
        playedTracks.removeAll { true }

        // Save Room
        ref.updateChildrenAsync(
            mapOf(
                "state" to state,
                "currentlyPlaying" to currentlyPlaying,
                "rounds" to rounds,
            )
        )
    }
}
