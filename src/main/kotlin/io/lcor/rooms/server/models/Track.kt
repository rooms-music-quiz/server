package io.lcor.rooms.server.models

import com.expediagroup.graphql.generator.annotations.GraphQLIgnore
import com.google.firebase.database.Exclude
import io.lcor.rooms.server.repositories.ArtistRepository
import io.lcor.rooms.server.utils.TrackSerializer
import kotlinx.serialization.Serializable
import java.util.*

@Serializable(with = TrackSerializer::class)
data class Track(
    var id: String = UUID.randomUUID().toString(),
    var name: String? = null,
    var artists: List<String> = listOf(),
    var images: List<Image> = listOf(),
    var previewUrl: String? = null,
    @GraphQLIgnore @Transient @Exclude var playlists: MutableList<String> = mutableListOf(),
) {

    @Exclude
    suspend fun artists(): List<Artist?> = artists.map { ArtistRepository.getArtistById(it) }

    @Exclude
    @GraphQLIgnore
    fun isValid(): Boolean = when {
        previewUrl.isNullOrBlank() -> false
        previewUrl == "null" -> false
        artists.isEmpty() -> false
        else -> true
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Track) return false
        return other.name == name && other.artists == artists
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + artists.hashCode()
        return result
    }

}
