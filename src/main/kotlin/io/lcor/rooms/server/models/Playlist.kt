package io.lcor.rooms.server.models

import com.expediagroup.graphql.generator.annotations.GraphQLIgnore
import com.google.firebase.database.Exclude
import io.lcor.rooms.server.repositories.TrackRepository
import io.lcor.rooms.server.utils.PlaylistSerializer
import kotlinx.serialization.Serializable
import java.util.*

@Serializable(with = PlaylistSerializer::class)
data class Playlist(
    var id: String = UUID.randomUUID().toString(),
    var name: String? = null,
    var description: String? = null,
    var images: List<Image> = listOf(),
    var tracks: List<String> = listOf(),
    @GraphQLIgnore @Transient @Exclude var rooms: MutableList<String> = mutableListOf(),
) {

    @Exclude
    suspend fun tracks(): List<Track?> = tracks.map { TrackRepository.getTrackById(it) }

    override fun equals(other: Any?): Boolean {
        if (other !is Playlist) return false
        return other.name == name && other.tracks == tracks
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + tracks.hashCode()
        return result
    }

}