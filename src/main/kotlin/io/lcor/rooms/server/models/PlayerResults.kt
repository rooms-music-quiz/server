package io.lcor.rooms.server.models

import io.lcor.rooms.server.utils.PlayerResultsSerializer
import kotlinx.serialization.Serializable

@Serializable(with = PlayerResultsSerializer::class)
data class PlayerResults(
    var playerId: String? = null,
    var track: Float = 0f,
    var artists: MutableList<Float>
) {
    constructor(playerId: String, artistNumber: Int) : this(
        playerId = playerId,
        track = 0f,
        artists = MutableList(artistNumber) { 0f }
    )
}
