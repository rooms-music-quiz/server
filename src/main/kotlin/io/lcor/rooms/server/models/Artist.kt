package io.lcor.rooms.server.models

import com.expediagroup.graphql.generator.annotations.GraphQLIgnore
import com.google.firebase.database.Exclude
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.util.*

@Serializable
data class Artist(
    var id: String = UUID.randomUUID().toString(),
    var name: String? = null,
    @GraphQLIgnore @Transient @Exclude var tracks: MutableList<String> = mutableListOf(),
) {

    override fun equals(other: Any?): Boolean {
        if (other !is Artist) return false
        return other.name == name
    }

    override fun hashCode(): Int {
        return name?.hashCode() ?: 0
    }
}
