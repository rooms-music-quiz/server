package io.lcor.rooms.server.models

import kotlinx.serialization.Serializable


@Serializable
data class GuessParameters(
    val title: String,
    val artists: List<String>,
    val input: String,
)
