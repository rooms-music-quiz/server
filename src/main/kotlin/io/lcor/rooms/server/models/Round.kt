package io.lcor.rooms.server.models

import com.expediagroup.graphql.generator.annotations.GraphQLIgnore
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Round(
    val scores: MutableList<PlayerResults>,
    val id: String = UUID.randomUUID().toString(),
    @GraphQLIgnore val startTime: Long = System.currentTimeMillis(),
) {
    constructor(players: List<String>, artistNumber: Int) : this(scores = players.map {
        PlayerResults(
            it,
            artistNumber
        )
    }.toMutableList())
}
