package io.lcor.rooms.server.models

import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Player(
    var id: String = UUID.randomUUID().toString(),
    var name: String? = null,
    var points: Int = 0
)
