package io.lcor.rooms.server.models

import kotlinx.serialization.Serializable

@Serializable
data class Image(
    var url: String? = null,
    var width: Int? = 0,
    var height: Int? = 0,
)
