package io.lcor.rooms.server.models

data class RoomParameters(
    val playlistId: String? = null,
    val genres: String? = null,
    val difficulty: Int? = null,
    val isPublic: Boolean? = true,
    val isRandom: Boolean? = false,
    val isImmutable: Boolean? = false,
    val locale: String? = null,
    val name: String? = "",
    val description: String? = "",
    val numbers: Int? = 20
) {

    fun isValid(): Boolean = !(playlistId == null && (genres == null || difficulty == null))


}