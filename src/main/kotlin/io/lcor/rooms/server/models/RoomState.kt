package io.lcor.rooms.server.models

enum class RoomState {

    LOBBY, INITIALIZATION, PLAYING, BUFFERING, TERMINATED,

}