package io.lcor.rooms.server

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.lcor.rooms.server.plugins.configureAuthentication
import io.lcor.rooms.server.plugins.configureFirebase
import io.lcor.rooms.server.plugins.configureLogging
import io.lcor.rooms.server.plugins.configureRouting
import io.lcor.rooms.server.repositories.ArtistRepository
import io.lcor.rooms.server.repositories.PlaylistRepository
import io.lcor.rooms.server.repositories.RoomRepository
import io.lcor.rooms.server.repositories.TrackRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


fun main() {

    val port = System.getenv("PORT")?.toIntOrNull() ?: 8000

    embeddedServer(Netty, port = port, host = "192.168.1.142") {

        configureLogging()
        configureFirebase()
        configureAuthentication()
        configureRouting()

        // Initialize repo
        RoomRepository.getAll()
        PlaylistRepository.getAll()
        TrackRepository.getAll()
        ArtistRepository.getAll()

        // Launch several public rooms, always accessible to players
        /*launch {

            // Today's Hit
            val hitRoom = RoomRepository.createRoom("37i9dQZF1DXdvyLcddcaVU", isPublic = true, isRandom = true)

            // Classic Indie hits
            val indieRoom = RoomRepository.createRoom("37i9dQZF1DWST4Eo6XmJX0", isPublic = true, isRandom = true)

            // Legendary hits
            val legendaryRoom = RoomRepository.createRoom("7oBeEkujcRybm7dCAUAIhG", isPublic = true, isRandom = true)

            delay(1000L)

            hitRoom.play()
            indieRoom.play()
            legendaryRoom.play()
        }*/

    }.start(wait = true)
}
