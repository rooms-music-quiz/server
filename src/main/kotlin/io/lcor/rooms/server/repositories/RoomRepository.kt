package io.lcor.rooms.server.repositories

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.lcor.rooms.server.models.GuessParameters
import io.lcor.rooms.server.models.PlayerResults
import io.lcor.rooms.server.models.Playlist
import io.lcor.rooms.server.models.Room
import io.lcor.rooms.server.utils.Constants
import io.lcor.rooms.server.utils.Log
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.serialization.json.Json

object RoomRepository {

    private const val FUNCTION_URL = "https://europe-west1-music-quiz-d2dad.cloudfunctions.net/matching"
    private val jsonDecoder = Json { ignoreUnknownKeys = true }

    // Firebase instance to retrieve rooms in database
    private val db = FirebaseDatabase.getInstance("https://rooms-music-quiz-d2dad.europe-west1.firebasedatabase.app/")
    private val ref = db.reference

    private val functionHttpClient = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    // Keep track of the rooms in the Database
    private val rooms: MutableStateFlow<MutableList<Room>> = MutableStateFlow(mutableListOf())

    // Automatic listening of rooms updates in the database
    init {
        ref.addChildEventListener(object : ChildEventListener {

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val newRoom = snapshot.getValue(Room::class.java) ?: return
                rooms.value.add(newRoom)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val updatedRoom = snapshot.getValue(Room::class.java) ?: return
                rooms.value.find { it.id == updatedRoom.id }?.apply {
                    id = updatedRoom.id
                    playlist = updatedRoom.playlist
                    players = updatedRoom.players
                    isRandom = updatedRoom.isRandom
                    isPublic = updatedRoom.isPublic
                    state = updatedRoom.state
                    currentlyPlaying = updatedRoom.currentlyPlaying
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val key = snapshot.key
                rooms.value.removeIf { it.id == key }
                PlaylistRepository.removeRoomFromPlaylist(key, snapshot.child("playlist").value as String)
            }

            override fun onChildMoved(snapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onCancelled(error: DatabaseError?) {
                error?.toException()?.let { Log.e(it) }
            }

        })
    }

    fun getRoomById(id: String): Room? = rooms.value.find { it.id == id }

    suspend fun createRoom(
        playlistId: String,
        isPublic: Boolean = true,
        isRandom: Boolean = false,
        isImmutable: Boolean = false
    ): Room {
        val newRef = ref.push()
        val room = Room(
            id = newRef.key,
            playlist = PlaylistRepository.addRoomToPlaylist(newRef.key, playlistId).id,
            isPublic = isPublic,
            isRandom = isRandom,
            isImmutable = isImmutable,
        )
        newRef.setValueAsync(room)
        return room
    }

    suspend fun createRoom(
        playlist: Playlist,
        isPublic: Boolean = true,
        isRandom: Boolean = false,
        isImmutable: Boolean = false
    ): Room {
        val newRef = ref.push()
        val room = Room(
            id = newRef.key,
            playlist = PlaylistRepository.addRoomToPlaylist(newRef.key, playlist).id,
            isPublic = isPublic,
            isRandom = isRandom,
            isImmutable = isImmutable,
        )
        newRef.setValueAsync(room)
        return room
    }

    fun removeRoomById(id: String): Boolean {
        val room = getRoomById(id) ?: return false
        room.stop()
        ref.child(id).removeValueAsync()
        return true
    }

    fun addPlayerToRoom(roomId: String, playerId: String): String? {
        val room = rooms.value.find { it.id == roomId } ?: return null
        val newRef = ref.child("${room.id}/players")
        return if (room.addPlayer(playerId)) {
            val playerList = room.players
            newRef.setValueAsync(playerList)
            playerId
        } else {
            null
        }
    }

    fun removePlayerFromRoom(roomId: String, playerId: String): Boolean {
        val room = rooms.value.find { it.id == roomId } ?: return false

        // Remove the player from the Room
        room.removePlayer(playerId)

        // remove player from the Room in the Db
        val playerList = room.players
        val newRef = ref.child("${room.id}/players")
        newRef.setValueAsync(playerList)

        if (room.players.isEmpty() && room.isImmutable) {
            // If the room is empty and is immutable, we restart it
            room.reset()
        } else if (room.players.isEmpty()) {
            // If the room is empty and is not immutable, we can delete it
            removeRoomById(roomId)
        }

        return true
    }

    fun startRoom(roomId: String): Boolean {
        val room = rooms.value.find { it.id == roomId } ?: return false
        room.play()
        return true
    }

    suspend fun guessTrack(
        roomId: String,
        playerId: String,
        title: String,
        artists: List<String>,
        input: String
    ): PlayerResults {
        val playerResults: PlayerResults = functionHttpClient.post<PlayerResults>(FUNCTION_URL) {
            contentType(ContentType.Application.Json)
            body = GuessParameters(title, artists, input)
        }.apply { this.playerId = playerId }
        val room = rooms.value.find { it.id == roomId } ?: return playerResults
        val elapsedTime: Float = (System.currentTimeMillis() - room.rounds.last().startTime) / 1000f
        val currentScore = room.rounds.last().scores.first { it.playerId == playerId }
        val newScore = playerResults.run {
            val newTrackResult = normalizeScore(this.track, elapsedTime)
            val newArtistsResults = this.artists.map { normalizeScore(it, elapsedTime) }
            copy(
                track = if (newTrackResult > currentScore.track) newTrackResult else currentScore.track,
                artists = if (newArtistsResults.sum() > currentScore.artists.sum()) newArtistsResults.toMutableList() else currentScore.artists
            )
        }
        updatePlayerScore(roomId, newScore)
        return playerResults
    }

    fun updatePlayerScore(roomId: String, score: PlayerResults): Boolean {
        val room = rooms.value.find { it.id == roomId } ?: return false
        return if (room.updatePlayerScore(score)) {
            ref.child("$roomId/rounds").setValueAsync(room.rounds)
            true
        } else false
    }

    fun getAll(showPrivate: Boolean = false) = rooms.asStateFlow().value.filter { it.isPublic || showPrivate }

    // Return a normalized score : 0 if under the score threshold, or a score depending on the elapsed time otherwise
    private fun normalizeScore(score: Float, elapsedTime: Float): Float {
        return if (score < Constants.RESULT_VALIDATION_THRESHOLD) 0f
        else Constants.DEFAULT_ROUND_DURATION / 1000 * score + (Constants.DEFAULT_ROUND_DURATION / 1000 - elapsedTime)
    }

}