package io.lcor.rooms.server.repositories

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import io.lcor.rooms.server.models.Image
import io.lcor.rooms.server.models.Track
import io.lcor.rooms.server.services.SpotifyService
import io.lcor.rooms.server.utils.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

object TrackRepository {

    // Firebase instances to retrieve playlists in database
    private val db = FirebaseDatabase.getInstance("https://track-music-quiz-d2dad.europe-west1.firebasedatabase.app/")
    private val ref = db.reference

    // Keep track of the playlists in the Database
    private val trackMutex: Mutex = Mutex()
    private val tracks: MutableStateFlow<MutableList<Track>> = MutableStateFlow(mutableListOf())

    // Automatic listening of rooms updates in the database
    init {
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                //Log.w("Snapshot is $snapshot", null)
                val newTrack = Track(
                    id = snapshot.key,
                    name = snapshot.child("name").value as String,
                    artists = snapshot.child("artists").value as List<String>,
                    images = snapshot.child("images").children.map {
                        Image(
                            url = it.child("url").value as String?,
                            width = (it.child("width").value as Long?)?.toInt(),
                            height = (it.child("height").value as Long?)?.toInt()
                        )
                    },
                    previewUrl = snapshot.child("previewUrl").value as String?,
                    playlists = snapshot.child("playlists").children.map { it.key }.toMutableList(),
                )
                //Log.w("Associated track is $newTrack", null)
                tracks.value.add(newTrack)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val updatedTrack = snapshot.getValue(Track::class.java) ?: return
                tracks.value.replaceAll { track ->
                    return@replaceAll if (track.id == updatedTrack.id) updatedTrack
                    else track
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val key = snapshot.key
                val oldTrack = tracks.value.firstOrNull { it.id == key } ?: return
                tracks.value.removeIf { it.id == key }
                runBlocking {
                    ArtistRepository.removeTrackFromArtist(key, *oldTrack.artists.toTypedArray())
                }
            }

            override fun onChildMoved(snapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onCancelled(error: DatabaseError?) {
                error?.toException()?.let { Log.e(it) }
            }

        })
    }

    suspend fun getTrackById(id: String): Track? = trackMutex.withLock {
        tracks.value.find { it.id == id }
    }

    private suspend fun createTrack(tracks: List<Track>): List<Track> {

        // Get artists first, so we can batch calls to the Spotify API
        val artistIds = tracks.flatMap { it.artists }.distinct()
        val artists = SpotifyService.getArtists(*artistIds.toTypedArray())

        tracks.forEach { track ->
            val newRef = ref.push()
            track.id = newRef.key
            track.artists = ArtistRepository
                .addTrackToArtist(track.id, artists.filter { track.artists.contains(it.id) })
                .map { it.id }
            val future = newRef.setValueAsync(track)
            while (!future.isDone) delay(1L)
        }
        return tracks
    }

    suspend fun addPlaylistToTrack(playlistId: String, playlistTracks: List<Track>): List<Track> {

        // Create tracks if necessary
        val tracksCpy = mutableListOf(*tracks.value.toTypedArray())
        val existingTracks = tracksCpy.filter { t -> playlistTracks.map { it.name }.contains(t.name) }
//        val newTracks = createTrack(playlistTracks.minus(existingTracks.toSet()))
        val newTracks = createTrack(playlistTracks.filterNot { t -> tracksCpy.map { it.name }.contains(t.name) })

        // Add the reference to the playlist
        val allNewTracks = existingTracks.plus(newTracks)
        allNewTracks.forEach { track ->
            getTrackById(track.id)?.playlists?.add(playlistId)
            ref.child("${track.id}/playlists/$playlistId").setValueAsync(true)
        }
        return allNewTracks.toList()
    }

    suspend fun addPlaylistToTrack(playlistId: String, vararg trackId: String): List<Track> {
        val tracks = SpotifyService.getTracks(*trackId)
        return addPlaylistToTrack(playlistId, tracks)
    }

    suspend fun removePlaylistFromTrack(playlistId: String, vararg trackId: String) {
        trackId.forEach {
            val track = getTrackById(it) ?: return@forEach
            track.playlists.remove(playlistId)

            if (track.playlists.isEmpty()) {
                removeTrackById(it)
            } else {
                ref.child("${track.id}/playlists/$playlistId").setValueAsync(null)
            }
        }
    }

    fun removeTrackById(id: String): Boolean {
        // Delete track from the Db
        ref.child(id).removeValueAsync()
        return true
    }

    fun getAll(): List<Track> = tracks.asStateFlow().value.toList()

}