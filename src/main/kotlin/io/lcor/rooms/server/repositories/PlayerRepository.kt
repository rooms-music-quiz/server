package io.lcor.rooms.server.repositories

import io.lcor.rooms.server.models.Player
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

object PlayerRepository {

    private val players: MutableStateFlow<Set<Player>> = MutableStateFlow(setOf())

    fun getPlayerById(id: String): Player? = players.value.find { it.id == id }

    fun getAll() = players.asStateFlow().value

}