package io.lcor.rooms.server.repositories

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import io.lcor.rooms.server.models.Artist
import io.lcor.rooms.server.services.SpotifyService
import io.lcor.rooms.server.utils.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

object ArtistRepository {

    // Firebase instances to retrieve playlists in database
    private val db = FirebaseDatabase.getInstance("https://artist-music-quiz-d2dad.europe-west1.firebasedatabase.app/")
    private val ref = db.reference

    // Keep track of the playlists in the Database
    private val artistMutex: Mutex = Mutex()
    private val artists: MutableStateFlow<MutableList<Artist>> = MutableStateFlow(mutableListOf())

    // Automatic listening of rooms updates in the database
    init {
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val newArtist = Artist(
                    id = snapshot.key,
                    name = snapshot.child("name").value as String?,
                    tracks = snapshot.child("tracks").children.map { it.key }.toMutableList()
                )
                artists.value.add(newArtist)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val updatedArtist = snapshot.getValue(Artist::class.java) ?: return
                artists.value.replaceAll { artist ->
                    return@replaceAll if (artist.id == updatedArtist.id) updatedArtist
                    else artist
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val key = snapshot.key
                artists.value.removeIf { it.id == key }
            }

            override fun onChildMoved(snapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onCancelled(error: DatabaseError?) {
                error?.toException()?.let { Log.e(it) }
            }

        })
    }

    suspend fun getArtistById(id: String): Artist? = artistMutex.withLock {
        artists.value.find { it.id == id }
    }

    private suspend fun createArtist(artists: List<Artist>): List<Artist> {
        artists.forEach { artist ->
            val newRef = ref.push()
            artist.id = newRef.key
            val future = newRef.setValueAsync(artist)
            while (!future.isDone) delay(1L)
        }
        return artists
    }

    suspend fun addTrackToArtist(trackId: String, trackArtists: List<Artist>): List<Artist> {

        // Create artists if necessary
//        val existingArtists = artists.value.intersect(trackArtists.toSet())
        val artistsCpy = mutableListOf(*artists.value.toTypedArray())
        val existingArtists = artistsCpy.filter { a -> trackArtists.map { it.name }.contains(a.name) }
//        val newArtists = createArtist(trackArtists.minus(existingArtists.toSet()))
        val newArtists = createArtist(trackArtists.filterNot { a -> artistsCpy.map { it.name }.contains(a.name) })

        // Add the reference to the playlist
        val allNewArtists = existingArtists.plus(newArtists)
        allNewArtists.forEach { artist ->
            getArtistById(artist.id)?.tracks?.add(trackId)
            ref.child("${artist.id}/tracks/$trackId").setValueAsync(true)
        }
        return allNewArtists.toList()
    }

    suspend fun addTrackToArtist(trackId: String, vararg artistId: String): List<Artist> {
        val artists = SpotifyService.getArtists(*artistId)
        return addTrackToArtist(trackId, artists)
    }

    suspend fun removeTrackFromArtist(trackId: String, vararg artistId: String) {
        artistId.forEach {
            val artist = getArtistById(it) ?: return@forEach
            artist.tracks.remove(trackId)

            if (artist.tracks.isEmpty()) {
                removeArtistById(it)
            } else {
                ref.child("${artist.id}/tracks/$trackId").setValueAsync(null)
            }
        }
    }

    suspend fun removeArtistById(id: String): Boolean {
        getArtistById(id) ?: return false
        ref.child(id).removeValueAsync()
        return true
    }

    fun getAll(): List<Artist> = artists.asStateFlow().value.toList()

}