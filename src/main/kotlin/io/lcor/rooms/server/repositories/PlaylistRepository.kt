package io.lcor.rooms.server.repositories

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import io.lcor.rooms.server.models.Image
import io.lcor.rooms.server.models.Playlist
import io.lcor.rooms.server.services.SpotifyService
import io.lcor.rooms.server.utils.Log
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.runBlocking

object PlaylistRepository {

    // Firebase instances to retrieve playlists in database
    private val db =
        FirebaseDatabase.getInstance("https://playlist-music-quiz-d2dad.europe-west1.firebasedatabase.app/")
    private val ref = db.reference

    // Keep track of the playlists in the Database
    private val playlists: MutableStateFlow<MutableList<Playlist>> = MutableStateFlow(mutableListOf())

    // Automatic listening of rooms updates in the database
    init {
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val newPlaylist = Playlist(
                    id = snapshot.key,
                    name = snapshot.child("name").value as String,
                    description = snapshot.child("description").value as String,
                    images = snapshot.child("images").children.map {
                        Image(
                            url = it.child("url").value as String?,
                            width = (it.child("width").value as Long?)?.toInt(),
                            height = (it.child("height").value as Long?)?.toInt()
                        )
                    },
                    tracks = snapshot.child("tracks").value as List<String>,
                    rooms = snapshot.child("rooms").children.map { it.key }.toMutableList(),
                )
                playlists.value.add(newPlaylist)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                // If the playlist doesn't have any associated room, no need to change the list,
                // the playlist will be remove soon
                if (!snapshot.hasChild("rooms")) return

                val updatedPlaylist = snapshot.getValue(Playlist::class.java) ?: return
                playlists.value.replaceAll { playlist ->
                    return@replaceAll if (playlist.id == updatedPlaylist.id) updatedPlaylist
                    else playlist
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val key = snapshot.key
                val oldPlaylist = playlists.value.firstOrNull { it.id == key } ?: return
                playlists.value.removeIf { it.id == key }
                runBlocking {
                    TrackRepository.removePlaylistFromTrack(oldPlaylist.id, *oldPlaylist.tracks.toTypedArray())
                }
            }

            override fun onChildMoved(snapshot: DataSnapshot?, previousChildName: String?) {}

            override fun onCancelled(error: DatabaseError?) {
                error?.toException()?.let { Log.e(it) }
            }

        })
    }

    fun getPlaylistById(id: String): Playlist? = playlists.value.find { it.id == id }

    suspend fun createPlaylist(playlistId: String): Playlist {
        val newRef = ref.push()

        // Save playlist in the Db
        val playlist = SpotifyService.getPlaylist(playlistId).apply {
            id = newRef.key
            tracks = TrackRepository.addPlaylistToTrack(newRef.key, *tracks.toTypedArray()).map { it.id }
        }

        // Save tracks in the Db
        newRef.setValueAsync(playlist)

        return playlist
    }

    suspend fun createPlaylist(
        name: String,
        genres: List<String>,
        difficulty: Int,
        description: String = "",
        numbers: Int,
        locale: String?
    ): Playlist {
        val newRef = ref.push()
        val playlist = SpotifyService.createPlaylist(name, genres, difficulty, description, numbers, locale).apply {
            id = newRef.key
            tracks = TrackRepository.addPlaylistToTrack(newRef.key, *tracks.toTypedArray()).map { it.id }
        }
        newRef.setValueAsync(playlist)
        return playlist
    }

    suspend fun addRoomToPlaylist(roomId: String, roomPlaylist: Playlist): Playlist {
        val existingPlaylist = playlists.value.find { it == roomPlaylist } ?: createPlaylist(roomPlaylist.id)

        // Add the reference to the playlist
        ref.child("${existingPlaylist.id}/rooms/$roomId").setValueAsync(true)
        existingPlaylist.rooms.add(roomId)
        return existingPlaylist
    }

    suspend fun addRoomToPlaylist(roomId: String, roomPlaylistId: String): Playlist {
        val playlist = getPlaylistById(roomPlaylistId) ?: SpotifyService.getPlaylist(roomPlaylistId)
        playlist.tracks = playlist.tracks().filter { it != null && it.isValid() }.map { it!!.id }
        return addRoomToPlaylist(roomId, playlist)
    }

    fun removeRoomFromPlaylist(roomId: String, playlistId: String) {

        val playlist = getPlaylistById(playlistId) ?: return
        playlist.rooms.remove(roomId)

        if (playlist.rooms.isEmpty()) {
            removePlaylistById(playlistId)
        } else {
            //playlists.value.find { it.id == playlistId }?.rooms?.removeIf { it == roomId }
            ref.child("${playlist.id}/rooms/$roomId").setValueAsync(null)
        }
    }

    fun removePlaylistById(id: String): Boolean {
        // Delete the playlist in the Db
        ref.child(id).removeValueAsync()

        return true
    }

    fun getAll(): List<Playlist> = playlists.asStateFlow().value.toList()

}