package io.lcor.rooms.server.utils

import io.ktor.util.*
import org.slf4j.Logger


object Log {

    lateinit var logger: Logger

    fun t(message: String) = logger.trace(message)
    fun t(obj: Any) = logger.trace(obj.toString())
    fun i(message: String) = logger.info(message)
    fun i(obj: Any) = logger.info(obj.toString())
    fun d(message: String) = logger.debug(message)
    fun d(obj: Any) = logger.debug(obj.toString())
    fun w(message: String, error: Throwable?) = logger.warn(message, error)
    fun w(obj: Any, error: Throwable?) = logger.warn(obj.toString(), error)
    fun e(message: String, error: Throwable?) = logger.error(message, error)
    fun e(error: Throwable) = logger.error(error)
    fun e(obj: Any, error: Throwable?) = logger.error(obj.toString(), error)


}