package io.lcor.rooms.server.utils

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.filter.Filter
import ch.qos.logback.core.spi.FilterReply

class FirebaseFilter : Filter<ILoggingEvent>() {

    companion object {
        const val FIREBASE_DATABASE_WORKER = "firebase-database-worker"
        const val FIREBASE_DATABASE_EVENT = "firebase-database-event-target"
        const val FIREBASE_WEBSOCKET_WORKER = "firebase-websocket-worker"
    }

    override fun decide(event: ILoggingEvent?): FilterReply {
        return when {
            event?.threadName == FIREBASE_DATABASE_WORKER && !event.level.isGreaterOrEqual(Level.WARN) -> FilterReply.DENY
            event?.threadName == FIREBASE_DATABASE_EVENT && !event.level.isGreaterOrEqual(Level.WARN) -> FilterReply.DENY
            event?.threadName == FIREBASE_WEBSOCKET_WORKER && !event.level.isGreaterOrEqual(Level.WARN) -> FilterReply.DENY
            else -> FilterReply.ACCEPT
        }
    }


}