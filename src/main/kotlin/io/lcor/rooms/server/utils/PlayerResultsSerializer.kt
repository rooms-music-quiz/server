package io.lcor.rooms.server.utils

import io.lcor.rooms.server.models.Image
import io.lcor.rooms.server.models.PlayerResults
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*
import io.lcor.rooms.server.models.Playlist
import io.lcor.rooms.server.models.Track


object PlayerResultsSerializer : KSerializer<PlayerResults> {

    private val jsonDecoder = Json { ignoreUnknownKeys = true }

    override val descriptor: SerialDescriptor =
        buildClassSerialDescriptor("PlayerResults") {
            element<String?>("playerId")
            element<Float>("track")
            element<List<Float>>("artists")
        }

    override fun serialize(encoder: Encoder, value: PlayerResults) {
        require(encoder is JsonEncoder)
        encoder.encodeJsonElement(buildJsonObject {
            put("playerId", value.playerId)
            put("track", value.track)
            put("artists", Json.encodeToJsonElement(value.artists))
        })
    }

    override fun deserialize(decoder: Decoder): PlayerResults {
        require(decoder is JsonDecoder)
        val root = decoder.decodeJsonElement()
        val element = root.jsonObject["data"]!!

        val artists =
            element.jsonObject["artists_scores"]!!.jsonArray.map { it.jsonArray.last().jsonPrimitive.content.toFloat() }

        return PlayerResults(
            playerId = element.jsonObject["playerId"]?.jsonPrimitive?.content,
            track = element.jsonObject["title_score"]!!.jsonPrimitive.content.toFloat(),
            artists = artists.toMutableList(),
        )
    }
}