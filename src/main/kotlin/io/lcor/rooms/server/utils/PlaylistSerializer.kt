package io.lcor.rooms.server.utils

import io.lcor.rooms.server.models.Image
import io.lcor.rooms.server.models.Playlist
import io.lcor.rooms.server.models.Track
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.*


object PlaylistSerializer : KSerializer<Playlist> {

    private val jsonDecoder = Json { ignoreUnknownKeys = true }

    override val descriptor: SerialDescriptor =
        buildClassSerialDescriptor("Playlist") {
            element<String>("id")
            element<String>("name")
            element<String>("description")
            element("tracks", buildClassSerialDescriptor("tracks") {
                element<Set<Track>>("items")
            })
            element<Set<Image>>("images")
        }

    override fun serialize(encoder: Encoder, value: Playlist) {
        require(encoder is JsonEncoder)
        encoder.encodeJsonElement(buildJsonObject {
            put("id", value.id)
            put("name", value.name)
            put("description", value.description)
            put("tracks", Json.encodeToJsonElement(value.tracks))
            put("images", Json.encodeToJsonElement(value.images))
        })
    }

    override fun deserialize(decoder: Decoder): Playlist {
        require(decoder is JsonDecoder)
        val root = decoder.decodeJsonElement()
        val element = root.jsonObject["tracks"]!!

        val tracks = element.jsonObject["items"]
            ?.jsonArray
            ?.onEach { Log.d("Element is $it") }
            ?.map { jsonDecoder.decodeFromJsonElement<Track>(it) }
            ?: listOf()

        return Playlist(
            id = root.jsonObject["id"]!!.jsonPrimitive.content,
            name = root.jsonObject["name"]!!.jsonPrimitive.content,
            description = root.jsonObject["description"]!!.jsonPrimitive.content,
            images = root.jsonObject["images"]!!.jsonArray.map { jsonDecoder.decodeFromJsonElement(it) },
            tracks = tracks
                .filter { it.isValid() }
                .map { it.id },
        )
    }
}