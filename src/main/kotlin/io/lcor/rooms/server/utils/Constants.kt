package io.lcor.rooms.server.utils

object Constants {

    const val DEFAULT_ROUND_DURATION = 30000L
    const val DEFAULT_INITIALIZATION_DURATION = 5000L
    const val DEFAULT_BREAK_DURATION = 10000L

    const val RESULT_VALIDATION_THRESHOLD = 0.8f
}