package io.lcor.rooms.server.utils

import com.google.cloud.secretmanager.v1.SecretManagerServiceClient
import com.google.cloud.secretmanager.v1.SecretVersionName

/**
 * Return a secret stored in Google Cloud Platform
 */
object SecretManager {

    private val secretClient = SecretManagerServiceClient.create()

    fun accessSecret(projectId: String?, secretId: String?, versionId: String?): String {

        val secretVersionName: SecretVersionName = SecretVersionName.of(projectId, secretId, versionId)
        val response = secretClient.accessSecretVersion(secretVersionName)
        return response.payload.data.toStringUtf8()

    }
}