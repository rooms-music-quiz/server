package io.lcor.rooms.server.utils

import io.lcor.rooms.server.models.Artist
import io.lcor.rooms.server.models.Image
import io.lcor.rooms.server.models.Track
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*


object TrackSerializer : KSerializer<Track> {

    private val jsonDecoder = Json { ignoreUnknownKeys = true }

    override val descriptor: SerialDescriptor =
        buildClassSerialDescriptor("Track") {
            element("track", buildClassSerialDescriptor("track") {
                element<String>("id")
                element<String>("name")
                element<Set<Artist>>("artists")
                element("album", buildClassSerialDescriptor("album") {
                    element<Set<Image>>("images")
                })
                element<String>("preview_url")
            })
        }

    override fun serialize(encoder: Encoder, value: Track) {
        require(encoder is JsonEncoder)
        encoder.encodeJsonElement(buildJsonObject {
            put("id", value.id)
            put("name", value.name)
            put("artists", Json.encodeToJsonElement(value.artists))
            put("images", Json.encodeToJsonElement(value.images))
            put("preview_url", value.previewUrl)
        })
    }

    override fun deserialize(decoder: Decoder): Track {
        require(decoder is JsonDecoder)
        val root = decoder.decodeJsonElement()
        val element = root.jsonObject["track"] ?: root
        return Track(
            id = element.jsonObject["id"]!!.jsonPrimitive.content,
            name = element.jsonObject["name"]!!.jsonPrimitive.content,
            artists = element.jsonObject["artists"]!!.jsonArray.map {
                jsonDecoder.decodeFromJsonElement<Artist>(it)
            }.map { it.id },
            images = element.jsonObject["album"]!!.jsonObject["images"]!!.jsonArray.map {
                jsonDecoder.decodeFromJsonElement<Image>(it)
            },
            previewUrl = element.jsonObject["preview_url"]!!.jsonPrimitive.content,
        )
    }
}