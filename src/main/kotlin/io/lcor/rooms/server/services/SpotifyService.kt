package io.lcor.rooms.server.services

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.auth.*
import io.ktor.client.features.auth.providers.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.lcor.rooms.server.models.Artist
import io.lcor.rooms.server.models.Playlist
import io.lcor.rooms.server.models.SpotifyAuthResponse
import io.lcor.rooms.server.models.Track
import io.lcor.rooms.server.utils.SecretManager
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.*
import java.util.*

private val jsonDecoder = Json { ignoreUnknownKeys = true }

object SpotifyService {

    private val clientID = SecretManager.accessSecret("1089855032998", "SPOTIFY_ROOMS_ID", "latest")

    //        System.getenv("SPOTIFY_CLIENT_ID") ?: throw ExceptionInInitializerError("Could not get Spotify client id")
    private val clientSecret = SecretManager.accessSecret("1089855032998", "SPOTIFY_ROOMS_SECRET", "latest")
//        System.getenv("SPOTIFY_CLIENT_SECRET")
//        ?: throw ExceptionInInitializerError("Could not get Spotify client secret")

    private const val spotifyAuthUrl = "https://accounts.spotify.com/api/token"
    private const val spotifyUrl = "https://api.spotify.com/v1"

    private val authCode = Base64.getEncoder().encodeToString("$clientID:$clientSecret".encodeToByteArray())

    // Http Client used to make request to get the Oauth token
    private val tokenClient = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    // Http Client used to make request to the Spotify API
    private val spotifyClient = HttpClient(CIO) {
        expectSuccess = false
        install(JsonFeature) {
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }
        install(Auth) {
            lateinit var tokenInfo: SpotifyAuthResponse
            var refreshTokenInfo: SpotifyAuthResponse
            bearer {
                loadTokens {
                    tokenInfo = getSpotifyToken()
                    BearerTokens(
                        accessToken = tokenInfo.accessToken, refreshToken = tokenInfo.accessToken
                    )
                }
                refreshTokens {
                    refreshTokenInfo = getSpotifyToken()
                    BearerTokens(
                        accessToken = refreshTokenInfo.accessToken, refreshToken = tokenInfo.accessToken
                    )
                }
            }
        }
    }

    /**
     * Get the Spotify authentication token
     */
    private suspend fun getSpotifyToken(): SpotifyAuthResponse = tokenClient.submitForm(spotifyAuthUrl,
        formParameters = Parameters.build { append("grant_type", "client_credentials") }) {
        headers {
            append(HttpHeaders.Authorization, "Basic $authCode")
        }
    }

    /**
     * Get the list of currently featured Spotify playlist
     */
    suspend fun getFeaturedPlaylist(country: String? = null, locale: String? = null): List<Playlist> {
        val rawResponse: HttpResponse = spotifyClient.get("$spotifyUrl/browse/featured-playlists") {
            parameter("country", country)
            parameter("locale", locale)
        }
        val playlistRawJson: JsonElement = Json.decodeFromString(rawResponse.receive())
        val playlistJson =
            playlistRawJson.jsonObject["playlists"]?.jsonObject?.get("items")?.jsonArray ?: return listOf()
        return Json.decodeFromJsonElement(playlistJson)
    }

    /**
     * Return details of several tracks
     */
    suspend fun getTracks(vararg idsParam: String): List<Track> {
        var ids = idsParam.clone()
        val tracks = mutableListOf<Track>()
        while (ids.isNotEmpty()) {
            val chunk = ids.take(50)
            val chunkIds = chunk.reduce { acc, s -> "$acc,$s" }
            val rawResponse: HttpResponse = spotifyClient.get("$spotifyUrl/tracks") {
                parameter("ids", chunkIds)
            }
            val tracksRawJson: JsonElement = Json.decodeFromString(rawResponse.receive())
            val tracksJson = tracksRawJson.jsonObject["tracks"]
                ?: throw IllegalArgumentException("Could not parse tracks from Spotify")
            tracks.addAll(Json.decodeFromJsonElement(tracksJson))
            ids = ids.drop(50).toTypedArray()
        }
        return tracks
    }

    /**
     * Return details of several artists
     */
    suspend fun getArtists(vararg idsParam: String): List<Artist> {
        var ids = idsParam.clone()
        val artists = mutableListOf<Artist>()
        while (ids.isNotEmpty()) {
            val chunk = ids.take(50)
            val chunkIds = chunk.reduce { acc, s -> "$acc,$s" }
            val rawResponse: HttpResponse = spotifyClient.get("$spotifyUrl/artists") {
                parameter("ids", chunkIds)
            }
            val artistsRawJson: JsonElement = Json.decodeFromString(rawResponse.receive())
            val artistsJson = artistsRawJson.jsonObject["artists"]
                ?: throw IllegalArgumentException("Could not parse artists from Spotify, got $artistsRawJson")
            artists.addAll(jsonDecoder.decodeFromJsonElement(artistsJson))
            ids = ids.drop(50).toTypedArray()
        }
        return artists
    }

    /**
     * Return the details of a specific Spotify playlist
     */
    suspend fun getPlaylist(id: String): Playlist = spotifyClient.get("$spotifyUrl/playlists/$id") {
        parameter("fields", "id,name,description,tracks,images")
    }

    suspend fun createPlaylist(
        name: String, genres: List<String>, difficulty: Int, description: String = "", numbers: Int, locale: String?
    ): Playlist {

        if (difficulty < 0 || difficulty > 100) throw IllegalArgumentException("Difficulty not acceptable")
        if (numbers < 1 || numbers > 100) throw IllegalArgumentException("Limit not acceptable")
        // TODO: Check the list of genres

        val tracks: MutableList<Track> = mutableListOf()
        var remainingNumber = numbers

        while (remainingNumber > 0) {
            val rawResponse: HttpResponse = spotifyClient.get("$spotifyUrl/recommendations") {
                parameter("seed_genres", genres.reduce { acc, s -> "$acc,$s" })
                parameter("min_popularity", difficulty)
                parameter("limit", remainingNumber)
                parameter("market", locale)
            }
            val tracksRawJson: JsonElement = Json.decodeFromString(rawResponse.receive())
            val tracksJson = tracksRawJson.jsonObject["tracks"]
                ?: throw IllegalArgumentException("Could not parse tracks from Spotify")

            val newTracks: MutableList<Track> = Json.decodeFromJsonElement(tracksJson)
            newTracks.removeIf { !it.isValid() }

            tracks.addAll(newTracks)
            remainingNumber -= newTracks.size
        }

        return Playlist(name = name, description = description, tracks = tracks.map { it.id })
    }

    /**
     * Close http client used by the repository
     */
    fun close() {
        tokenClient.close()
        spotifyClient.close()
    }
}