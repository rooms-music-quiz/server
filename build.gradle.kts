val ktorVersion: String by project
val kotlinVersion: String by project
val logbackVersion: String by project
val graphQLVersion: String by project

plugins {
    application
    kotlin("jvm") version "1.6.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.6.0"
    id("com.github.johnrengelman.shadow") version "7.0.0"
    id("com.google.cloud.tools.appengine") version "2.4.2"
}

group = "io.lcor.rooms.server"
version = "0.0.1"
application {
    mainClass.set("io.lcor.rooms.server.ApplicationKt")
}

appengine {
    stage {
        setAppEngineDirectory("appengine")
        setArtifact("build/libs/${project.name}-${project.version}-all.jar")
    }
    deploy {
        version = "GCLOUD_CONFIG"
        projectId = "GCLOUD_CONFIG"
    }
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap")
        name = "ktor-eap"
    }
}

dependencies {


    // Ktor server dependencies
    implementation("io.ktor", "ktor-server-core", ktorVersion)
    implementation("io.ktor", "ktor-server-netty", ktorVersion)
    implementation("io.ktor", "ktor-serialization", ktorVersion)
    implementation("io.ktor", "ktor-auth", ktorVersion)
    implementation("io.ktor", "ktor-auth-jwt", ktorVersion)

    implementation("com.expediagroup", "graphql-kotlin-spring-server", graphQLVersion)
    implementation("ch.qos.logback", "logback-classic", logbackVersion)

    // Ktor client dependencies
    implementation("io.ktor", "ktor-client-core", ktorVersion)
    implementation("io.ktor", "ktor-client-cio", ktorVersion)
    implementation("io.ktor", "ktor-client-serialization", ktorVersion)
    implementation("io.ktor", "ktor-client-auth", ktorVersion)

    // Firebase dependencies
    implementation("com.google.firebase", "firebase-admin", "8.1.0")

    // Secret manager dependencies
    implementation(platform("com.google.cloud:libraries-bom:24.1.1"))
    implementation("com.google.cloud", "google-cloud-secretmanager")

    // Test dependencies
    testImplementation("org.jetbrains.kotlin", "kotlin-test-junit", kotlinVersion)
    testImplementation("io.ktor", "ktor-server-tests", ktorVersion)
}